/*
984. String Without AAA or BBB
Given two integers A and B, return any string S such that:

S has length A + B and contains exactly A 'a' letters, and exactly B 'b' letters;
The substring 'aaa' does not occur in S;
The substring 'bbb' does not occur in S.

Example 1:

Input: A = 1, B = 2
Output: "abb"
Explanation: "abb", "bab" and "bba" are all correct answers.
Example 2:

Input: A = 4, B = 1
Output: "aabaa"

Note:

0 <= A <= 100
0 <= B <= 100
It is guaranteed such an S exists for the given A and B.
*/

/*
Notice that the best sequence is aab if A>B:
    even if A=2B+2, we can handle it with aab,aab,aab,aab....,aab,aa.
We firstly apply this sequence until A=B then we use ab sequence instead.
Or we may run out of a/b: put remaining a/b at the last of the sequence.

*/


public:
    string strWithout3a3b(int A, int B) {
        string ans = "";
        while(A>0||B>0){
            if(A!=0&&B!=0){
                if(A>B){
                    ans+="aab";
                    A-=2;
                    B--;
                }
                else if(A<B){
                    ans+="bba";
                    A--;
                    B-=2;
                }
                else{
                    ans+="ab";
                    A--;
                    B--;
                }
            }
            else if(A==0){
                ans+="b";
                B--;
            }
            else{
                ans+="a";
                A--;
            }
        }
        return ans;
    }
};