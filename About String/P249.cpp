/*
249. Group Shifted Strings
Given a string, we can "shift" each of its letter to its successive letter, for example: "abc" -> "bcd". We can keep "shifting" which forms the sequence:

"abc" -> "bcd" -> ... -> "xyz"
Given a list of strings which contains only lowercase alphabets, group all strings that belong to the same shifting sequence.

Example:

Input: ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"],
Output: 
[
  ["abc","bcd","xyz"],
  ["az","ba"],
  ["acef"],
  ["a","z"]
]
*/

/*
    Notice that we can classify these  strings by the difference sequence between s[i] and s[0].
    Notice that 'a'-'z' should be 1 as well:
        we  define  s[i]-s[0]=s[i]-s[0]+26 when s[i]<s[0].
    We can then store the sequence in a vector.


*/

class Solution {
public:
    vector<vector<string>> groupStrings(vector<string>& strings) {
        map<vector<int>,vector<string>> mymap;
        for(auto i: strings){
            vector<int> temp;
            for(auto j:i){
                temp.push_back((j-i[0])<=0?j-i[0]+26:j-i[0]);
            }
            if(mymap.find(temp)!=mymap.end())
                mymap[temp].push_back(i);
            else mymap.insert(pair<vector<int>,vector<string>>(temp,{i}));
        }
        vector<vector<string>> ans;
        for(auto i:mymap)
            ans.push_back(i.second);
        return ans;
    }
};