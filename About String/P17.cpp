/*
17. Letter Combinations of a Phone Number
Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.

A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.
Example:

Input: "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
*/


/*
    Basic loop about string.
    
*/


class Solution {
public:
    vector<vector<string>> table{{"a","b","c"},{"d","e","f"},{"g","h","i"},{"j","k","l"},{"m","n","o"},{"p","q","r","s"},{"t","u","v"},{"w","x","y","z"}};
    vector<string> letterCombinations(string digits) {
        if(digits=="")
            return {};
        for(auto i:digits){
            if(i=='0'||i=='1')
                return {};
        }
        return helper(digits,digits.size()-1);
    }
    vector<string> helper(string digits, int p){
        vector<string> ans;
        if(p==0){
            for(auto i:table[(int)digits[0]-50]){
                ans.push_back(i);
            }
            return ans;
        }
        vector<string> lastans=helper(digits, p-1);
        for(auto i:lastans){
            for(auto j:table[(int)digits[p]-50]){
                ans.push_back(i+j);
            }
        }
        return ans;    
    }
};