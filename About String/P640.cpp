/*
640. Solve the Equation
Solve a given equation and return the value of x in the form of string "x=#value". The equation contains only '+', '-' operation, the variable x and its coefficient.

If there is no solution for the equation, return "No solution".

If there are infinite solutions for the equation, return "Infinite solutions".

If there is exactly one solution for the equation, we ensure that the value of x is an integer.

Example 1:

Input: "x+5-3+x=6+x-2"
Output: "x=2"
Example 2:

Input: "x=x"
Output: "Infinite solutions"
Example 3:

Input: "2x=x"
Output: "x=0"
Example 4:

Input: "2x+3x-6x=x+2"
Output: "x=-1"
Example 5:

Input: "x=x+2"
Output: "No solution"
*/

/*
    store 4 value:
    x[0] x[1] c[0] c[1]:
    the prefix of x left, the prefix of x right, the constant part left, the constant part right
    Then visit the hole whole string:
    if we meet number, continue;
    if we meet 'x', count the value to prefix of x;
               '-'/'+', count the value to constant part; may change sign;
               '=' count the value to constant part; change side.
    



*/

class Solution {
public:
    int x[2]={0,0};
    int c[2]={0,0};
    string solveEquation(string equation) {
        int len=equation.size();
        int side=0;
        int sign=1;
        for(int i=0;i<len;){
            for(int j=0;i+j<len;j++){
                if(equation[i+j]=='x'){
                    if(equation.substr(i,j)==""){
                        x[side]+=1*sign;
                        sign=1;
                    }
                    else{
                        x[side]+=sign*stoi(equation.substr(i,j));
                    }
                    i+=j+1;
                    break;
                }
                if(equation[i+j]=='-'){
                    if(equation.substr(i,j)!=""){
                        c[side]+=sign*stoi(equation.substr(i,j));
                    }
                    sign=-1;
                    i+=j+1;
                    break;
                }
                if(equation[i+j]=='+'){
                    if(equation.substr(i,j)!=""){
                        c[side]+=sign*stoi(equation.substr(i,j));
                    }
                    sign=1;
                    i+=j+1;
                    break;
                }
                if(i+j==len-1){
                    c[side]+=sign*stoi(equation.substr(i,j+1));
                    sign=1;
                    i+=j+1;
                    break;
                }
                if(equation[i+j]=='='){
                    if(equation.substr(i,j)!=""){
                        c[side]+=sign*stoi(equation.substr(i,j));
                    }
                    side=1; 
                    sign=1;
                    i+=j+1;
                    break;
                }
            }
        }
        //cout<<x[0]<<c[0]<<x[0]<<c[1];
        int xn=x[0]-x[1],cn=c[1]-c[0];
        if(xn==0){
            if(cn==0)
                return "Infinite solutions";
            else return "No solution";
        }
        else return "x="+to_string(cn/xn);
    }
};