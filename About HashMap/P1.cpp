/**
1. Two Sum
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
*/

/**
Find the corresponding target-nums[i] then return.
Use a hashmap to record it.


*/


class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        map<int,int> mymap;
        for(int i=0;i<nums.size();i++)
        {
            if(mymap.find(target-nums[i])!=mymap.end())
                return {mymap[target-nums[i]],i};
            else
                mymap.insert(pair<int,int>(nums[i],i));
        }
        return {};
    }
};