/**
This project is used to record my personal solutions in leetcode.
Some explainations will be given.

Language: C++

Generally, I will put following code at the top of the solution to accelate outputting.
This function will stop iostream from using buffer memory.

该项目用于在leetcode中记录我的个人解决方案。
将给出一些解释。
语言：C ++
通常，我会在解决方案的顶部放置以下代码来加速输出。
此函数将阻止iostream使用缓冲区内存。
**/

static const auto ___ = [](){
    std::cout.sync_with_stdio(false);
    cin.tie(0);
    return 0;
}();