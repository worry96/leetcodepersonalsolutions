/**
92. Reverse Linked List II
Reverse a linked list from position m to n. Do it in one-pass.

Note: 1 ≤ m ≤ n ≤ length of list.

Example:

Input: 1->2->3->4->5->NULL, m = 2, n = 4
Output: 1->4->3->2->5->NULL
*/

/**
Pretty easy to understand.
find the parts to reverse and do it.
*/


class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        auto runner=head;
        for(int i=1;i<m;i++)
            runner=runner->next;
        auto Lm=runner;
        vector<int> nums(n-m+1);
        for(int i=0;i<=n-m;i++){
            nums[i]=runner->val;
            runner=runner->next;
        }
        runner=Lm;
        for(int i=n-m;i>=0;i--){
            runner->val=nums[i];
            runner=runner->next;
        }
        return head;
    }
};