/**
19. Remove Nth Node From End of List
Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?
*/

/**
The distance between runner and recorder will be always n-1.
*/


class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        auto runner=head;
        for(int i=0;i<n;i++){
            runner=runner->next;
        }
        auto recorder=head;
        if(!runner) return head->next;
        while(runner->next){
            runner=runner->next;
            recorder=recorder->next;
        }
        recorder->next=recorder->next->next;
        return head;
    }
};