/*
142. Linked List Cycle II
Given a linked list, return the node where the cycle begins. If there is no cycle, return null.

To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.

Note: Do not modify the linked list.

 

Example 1:

Input: head = [3,2,0,-4], pos = 1
Output: tail connects to node index 1
Explanation: There is a cycle in the linked list, where tail connects to the second node.


Example 2:

Input: head = [1,2], pos = 0
Output: tail connects to node index 0
Explanation: There is a cycle in the linked list, where tail connects to the first node.


Example 3:

Input: head = [1], pos = -1
Output: no cycle
Explanation: There is no cycle in the linked list.


 

Follow up:
Can you solve it without using extra space?

*/



class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if(!head) return NULL;
        ListNode* slow = head, *fast = head;
        do{
            if(fast->next==NULL||fast->next->next==NULL)
                return NULL;
            slow=slow->next;
            fast=fast->next->next; 
        }while(slow!=fast);
        slow=head;
        while(slow!=fast){
            slow=slow->next;
            fast=fast->next;
        }
        return slow;
    }
/*
    Create a slow(1 step every time) and a fast runner(2 steps)
    Assume: number of nodes before loop: a;
            number of nodes in the loop: b;
    Their distance d will be 0,1,2,3...
    The steps slow runner has travelled is also d.
    When the distance is times of b, they meet.
    -->d%b==0
    then the distance from head to slow(fast) is: d-k*b(travelled k loops)
    Because (d-k*b)%b==0:
        Let slow be head then the distance will be d-k*b
        Push them forword they will meet again when slow get into the loop.
*/
};

