138. Copy List with Random Pointer
A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

Return a deep copy of the list.

Example 1:
Input:
{"$id":"1","next":{"$id":"2","next":null,"random":{"$ref":"2"},"val":2},"random":{"$ref":"2"},"val":1}

Explanation:
Node 1's value is 1, both of its next and random pointer points to Node 2.
Node 2's value is 2, its next pointer points to null and its random pointer points to itself.

/**
Careful about the meaning of deep copy.
*/


class Solution {
public:
    RandomListNode *copyRandomList(RandomListNode *head) {
        if (!head) {
            return NULL;                
        }
        RandomListNode *runner = head, *following = head;
        while (runner){
            following = runner->next;
            runner->next = new RandomListNode (runner->label);
            runner->next->next = following;
            runner = following;
        }
        runner = head;
        while (runner) {
            if (runner->random) {
                runner->next->random = runner->random->next;
            }            
            runner = runner->next->next;
        }
        auto new_head = head->next;
        runner = head;
        while (runner) {
            following = runner->next->next;
            if (following) {
                runner->next->next = following->next;
            }
            runner->next = following;
            runner = following;
        }
        
        return new_head;
    }
};