/**
114. Flatten Binary Tree to Linked List
Given a binary tree, flatten it to a linked list in-place.

For example, given the following tree:

    1
   / \
  2   5
 / \   \
3   4   6
The flattened tree should look like:

1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6
*/

/**
Step 1: preorder traversal those elements of root.
Step 2: generate a new tree.

*/


class Solution {
public:
    vector<int> v;
    void flatten(TreeNode* root) {
        if(!root) return;
        Helper(root);
        TreeNode* newt=new TreeNode (v[0]);
        TreeNode* runner=newt;
        for(int i=1;i<v.size();i++){
            runner->right=new TreeNode (v[i]);
            runner=runner->right;
        }
        *root=*newt;
        return;
    }
    void Helper(TreeNode* root){
        if(root){
            v.push_back(root->val);
            Helper(root->left);
            Helper(root->right);
        }
    }
};