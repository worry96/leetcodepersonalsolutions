/**
141. Linked List Cycle
Given a linked list, determine if it has a cycle in it.

To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.

 

Example 1:

Input: head = [3,2,0,-4], pos = 1
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the second node.


Example 2:

Input: head = [1,2], pos = 0
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the first node.


Example 3:

Input: head = [1], pos = -1
Output: false
Explanation: There is no cycle in the linked list.

Follow up:

Can you solve it using O(1) (i.e. constant) memory?
*/


/**
Similar to idea in P142.
We have walker and runner which moves 1/2 steps each time respectively.
--->Distance will increase by 1 each time.
--->At a certain time distance will be multiples of the length of circle
--->They meet each other.
*/


class Solution {
public:
    bool hasCycle(ListNode *head) {
    if(head==NULL) return false;
    ListNode *walker = head;
    ListNode *runner = head;
    while(runner->next!=NULL && runner->next->next!=NULL) {
        walker = walker->next;
        runner = runner->next->next;
        if(walker==runner) return true;
    }
    return false;
    }
};