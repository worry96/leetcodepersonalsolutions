/**
21. Merge Two Sorted Lists
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: 1->2->4, 1->3->4
Output: 1->1->2->3->4->4
*/

/**
Easy to understand.
The logic used in merge sort.
*/

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        // without head node
        ListNode* res = l1;
        ListNode* l1_p = NULL;
        while(l1!=NULL && l2!=NULL){
            if(l1->val <= l2->val){
                l1_p = l1;
                l1 = l1->next;
            }else{
                ListNode* l2_next = l2->next;
                l2->next = l1;
                if(l1_p!=NULL){
                    l1_p->next = l2;
                }else{
                    res = l2;
                }
                l1_p = l2;
                l2=l2_next;
            }
        }

        if(res == NULL){ // while l1 == NULL
            return l2;
        }else{
            if(l2){
                l1_p->next=l2;
            }
            return res;
        }
    }
};