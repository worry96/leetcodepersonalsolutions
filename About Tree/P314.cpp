/*
314. Binary Tree Vertical Order Traversal
Given a binary tree, return the vertical order traversal of its nodes' values. (ie, from top to bottom, column by column).

If two nodes are in the same row and column, the order should be from left to right.

Examples 1:
Input: [3,9,20,null,null,15,7]

   3
  /\
 /  \
 9  20
    /\
   /  \
  15   7 

Output:

[
  [9],
  [3,15],
  [20],
  [7]
]
Examples 2:
Input: [3,9,8,4,0,1,7]

     3
    /\
   /  \
   9   8
  /\  /\
 /  \/  \
 4  01   7 

Output:
[
  [4],
  [9],
  [3,0,1],
  [8],
  [7]
]
Examples 3:
Input: [3,9,8,4,0,1,7,null,null,null,2,5] (0's right child is 2 and 1's left child is 5)

     3
    /\
   /  \
   9   8
  /\  /\
 /  \/  \
 4  01   7
    /\
   /  \
   5   2

Output:
[
  [4],
  [9,5],
  [3,0,1],
  [8,2],
  [7]
]
*/

/*
Notice that we should visit the tree by level:
We create a vector: layer and the next layer to visit it by level.
Then we record the row index for each node:
if we go left index-=1; if we go right, index+=1.
*/





class Solution {
public:
    vector<vector<int>> verticalOrder(TreeNode* root) {
        if(!root) return {};
        map<int,vector<int>> mymap;
        vector<pair<TreeNode*,int>> layer={pair<TreeNode*,int>(root,0)};
        vector<pair<TreeNode*,int>> nextlayer;
        while(layer.size()!=0){
            for(auto i:layer){
                if(mymap.find(i.second)!=mymap.end())
                    mymap[i.second].push_back(i.first->val);
                else mymap.insert(pair<int,vector<int>>(i.second,{i.first->val}));
                if(i.first->left){
                    nextlayer.push_back(pair<TreeNode*,int>(i.first->left,i.second-1));
                }
                if(i.first->right){
                    nextlayer.push_back(pair<TreeNode*,int>(i.first->right,i.second+1));
                }
            }
            layer=nextlayer;
            nextlayer.clear();
        }
        vector<vector<int>> ans;
        for(auto i:mymap){
            ans.push_back(i.second);
        }
        return ans;
    }