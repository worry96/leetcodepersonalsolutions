/*
102. Binary Tree Level Order Traversal
Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).

For example:
Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its level order traversal as:

[
  [3],
  [9,20],
  [15,7]
]
*/

/*
    level by level order: process next layer only if we finish last layer.
    We can iterate this loop:
    Store nodes in next layer in a collection like queue/vector,  when we finish this layer,
    assign next layer to current layer then clear next layer.
*/

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        if(!root) return {};
        vector<TreeNode*> layer{root};
        vector<TreeNode*> nextlayer;
        vector<vector<int>> ans;
        while(layer.size()!=0){
            vector<int> temp;
            for(auto i:layer){
                temp.push_back(i->val);
                if(i->left)nextlayer.push_back(i->left);
                if(i->right)nextlayer.push_back(i->right);
            }
            ans.push_back(temp);
            layer=nextlayer;
            nextlayer.clear();
        }
        return ans;
    }
};
